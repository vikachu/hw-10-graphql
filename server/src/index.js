import { GraphQLServer } from "graphql-yoga";
import { prisma } from "./generated/prisma-client";
import * as Query from "./resolvers/Query";
import * as Mutation from "./resolvers/Mutation";
import * as Subscription from "./resolvers/Subscription";
import * as Message from "./resolvers/Message";
import * as Reply from "./resolvers/Reply";

const resolvers = {
  Query,
  Message,
  Reply,
  Mutation,
  Subscription,
};

const server = new GraphQLServer({
  typeDefs: "./src/schema.graphql",
  resolvers,
  context: { prisma },
});

server.start(() => console.log("App is listening on port 4000."));
