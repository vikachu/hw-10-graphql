const postMessage = (parent, args, context, info) => {
  return context.prisma.createMessage({
    text: args.text,
  });
};

async function postReply(parent, args, context, info) {
  const messageExists = await context.prisma.$exists.message({
    id: args.messageId,
  });

  if (!messageExists) {
    throw new Error("Message doesn't exists");
  }

  return context.prisma.createReply({
    text: args.text,
    message: { connect: { id: args.messageId } },
  });
}

async function likeMessage(parent, args, context, info) {
  const message = await context.prisma.message({
    id: args.messageId,
  });

  if (!message) {
    throw new Error("Message doesn't exists");
  }

  return context.prisma.updateMessage({
    where: { id: args.messageId },
    data: { likes: message.likes + 1 },
  });
}

async function dislikeMessage(parent, args, context, info) {
  const message = await context.prisma.message({
    id: args.messageId,
  });

  if (!message) {
    throw new Error("Message doesn't exists");
  }

  return context.prisma.updateMessage({
    where: { id: args.messageId },
    data: { dislikes: message.dislikes + 1 },
  });
}

export { postMessage, postReply, likeMessage, dislikeMessage };
